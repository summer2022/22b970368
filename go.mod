module openeuler.org/KubeOS

go 1.15

require (
	github.com/Microsoft/go-winio v0.5.2 // indirect
	github.com/containerd/continuity v0.3.0 // indirect
	github.com/docker/docker v17.12.0-ce+incompatible
	github.com/agiledragon/gomonkey v2.1.0+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/go-logr/logr v0.4.0
	github.com/gotestyourself/gotestyourself v2.2.0+incompatible // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
	github.com/opencontainers/runc v1.0.0-rc7 // indirect
	github.com/opencontainers/selinux v1.10.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/grpc v1.28.0
	google.golang.org/protobuf v1.27.1
	k8s.io/api v0.20.2
	k8s.io/apimachinery v0.20.2
	k8s.io/client-go v0.20.2
	k8s.io/kubectl v0.20.2
	sigs.k8s.io/controller-runtime v0.8.3
)
